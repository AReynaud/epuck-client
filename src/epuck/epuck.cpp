#include <epuck.h>

/* Variables for threads and sockets */
typedef struct sockaddr_in SOCKADDR_IN;
SOCKADDR_IN SockaddrinInit, local_SockaddrinInit, SockaddrinCamera,
    SockaddrinReceptionCapteurs, local_SockaddrinEnvoieCommandes,
    SockaddrinEnvoieCommandes;
SOCKET SockInit;
typedef struct sockaddr SOCKADDR;

// General functions
void sigint_handler(int signal) {
    stop_threads = true;
}

inline int sign(float val) {
    if (val < 0)
        return -1;
    if (val == 0)
        return 0;
    return 1;
}

// Functions for opening sockets
void InitSocketOpening(const std::string& epuck_ip) {
    printf("Creating socket Init :\n\r");
    static const int PortsockInit = 1029;
    SockInit = socket(AF_INET, SOCK_DGRAM, 0);
    if (SockInit == INVALID_SOCKET) {
        printf("Desole, je ne peux pas creer la socket port: %d \n",
               PortsockInit);
    } else {
        printf("socket port %d : OK \n", PortsockInit);
        /* Lie la socket � une ip et un port d'�coute */
        // callee (epuck) -> distant socket where I need to send to epuck
        SockaddrinInit.sin_family = AF_INET;
        SockaddrinInit.sin_addr.s_addr =
            inet_addr(epuck_ip.c_str()); // IP de l'epuck
        SockaddrinInit.sin_port =
            htons(PortsockInit); // Ecoute ou emet sur le PORT
        // caller (local PC)
        local_SockaddrinInit.sin_family = AF_INET;
        local_SockaddrinInit.sin_addr.s_addr =
            htonl(INADDR_ANY); // IP de l'epuck
        local_SockaddrinInit.sin_port =
            htons(PortsockInit); // Ecoute ou emet sur le PORT
        int erreur = bind(SockInit, (SOCKADDR*)&local_SockaddrinInit,
                          sizeof(local_SockaddrinInit));
        if (erreur == SOCKET_ERROR) {
            perror("ERROR Sock INIT :");
            printf("Echec du Bind de la socket port : %d \n", PortsockInit);
        } else {
            printf("bind : OK port : %d \n", PortsockInit);
        }
    }
}
void OpenCameraSocket() {
    printf("Creation Camera socket :\n\r");
    CameraSocket = socket(AF_INET, SOCK_DGRAM, 0);
    static const int PortsockCamera = 1026;

    if (CameraSocket == INVALID_SOCKET) {
        printf("Desole, je ne peux pas creer la socket port: %d \n",
               PortsockCamera);
    } else {
        printf("socket port %d : OK \n", PortsockCamera);
        /* Lie la socket � une ip et un port d'�coute */
        SockaddrinCamera.sin_family = AF_INET;
        SockaddrinCamera.sin_addr.s_addr = htonl(INADDR_ANY); // IP
        SockaddrinCamera.sin_port =
            htons(PortsockCamera); // Ecoute ou emet sur le PORT
        int erreur = bind(CameraSocket, (SOCKADDR*)&SockaddrinCamera,
                          sizeof(SockaddrinCamera));
        if (erreur == SOCKET_ERROR) {
            printf("Echec du Bind de la socket port : %d \n", PortsockCamera);
        } else {
            printf("bind : OK port : %d \n", PortsockCamera);
        }
    }
}
void OpenSensorReceivingSocket() {
    printf("Creation socket ReceptionCapteurs :\n\r");
    static const int PortsockReceptionCapteurs = 1028;
    SensorReceivingSocket = socket(AF_INET, SOCK_DGRAM, 0);
    if (SensorReceivingSocket == INVALID_SOCKET) {
        printf("Desole, je ne peux pas creer la socket port: %d \n",
               PortsockReceptionCapteurs);
    } else {
        printf("socket port %d : OK \n", PortsockReceptionCapteurs);
        /* Lie la socket � une ip et un port d'�coute*/
        SockaddrinReceptionCapteurs.sin_family = AF_INET;
        SockaddrinReceptionCapteurs.sin_addr.s_addr = htonl(INADDR_ANY); // IP
        SockaddrinReceptionCapteurs.sin_port =
            htons(PortsockReceptionCapteurs); // Ecoute ou emet sur le PORT
        int erreur =
            bind(SensorReceivingSocket, (SOCKADDR*)&SockaddrinReceptionCapteurs,
                 sizeof(SockaddrinReceptionCapteurs));
        if (erreur == SOCKET_ERROR) {
            printf("Echec du Bind de la socket port : %d \n",
                   PortsockReceptionCapteurs);
        } else {
            printf("bind : OK port : %d \n", PortsockReceptionCapteurs);
        }
    }
}
/* Socket EnvoieCommandes */
void OpenCommandSendingSocket(const std::string& epuck_ip) {
    printf("Creation socket EnvoieCommandes :\n\r");
    static const int PortsockEnvoieCommandes = 1027;
    CommandSendingSocket = socket(AF_INET, SOCK_DGRAM, 0);
    if (CommandSendingSocket == INVALID_SOCKET) {
        printf("Desole, je ne peux pas creer la socket port: %d \n",
               PortsockEnvoieCommandes);
    } else {
        printf("socket port %d : OK \n", PortsockEnvoieCommandes);
        /* Lie la socket � une ip et un port d'�coute */
        // callee (epuck) -> distant socket where I need to send to epuck
        SockaddrinEnvoieCommandes.sin_family = AF_INET;
        SockaddrinEnvoieCommandes.sin_addr.s_addr =
            inet_addr(epuck_ip.c_str()); // IP
        SockaddrinEnvoieCommandes.sin_port =
            htons(PortsockEnvoieCommandes); // Ecoute ou emet sur le PORT
        // caller (local PC)
        local_SockaddrinEnvoieCommandes.sin_family = AF_INET;
        local_SockaddrinEnvoieCommandes.sin_addr.s_addr =
            htonl(INADDR_ANY); // IP de l'epuck
        local_SockaddrinEnvoieCommandes.sin_port =
            htons(PortsockEnvoieCommandes); // Ecoute ou emet sur le PORT
        int erreur = bind(CommandSendingSocket,
                          (SOCKADDR*)&local_SockaddrinEnvoieCommandes,
                          sizeof(local_SockaddrinEnvoieCommandes));
        if (erreur == SOCKET_ERROR) {
            printf("Echec du Bind de la socket port : %d \n",
                   PortsockEnvoieCommandes);
        } else {
            printf("bind : OK port : %d \n", PortsockEnvoieCommandes);
        }
    }
}
/**** Fonction de fermeture de socket ****/
void CloseSocket(int NOM_SOCKET) {
    shutdown(NOM_SOCKET, 2); // Ferme la session d'emmission et d'�coute
    close(NOM_SOCKET);       // Ferme la socket
}
void ProcessInfraRed() {
    // infrarouges
    for (int i = 0; i < 10; i++) {
        printf("IRed %d : %i  ", i, CapteurProx[i]);
    }
    printf("\n");
	float dist[8];
    for (int i = 0; i < 8; i++) {
        dist[i] = 1.79 * pow(CapteurProx[i], -.681);
    }
    for (int i = 8; i < 10; i++) {
        dist[i] = 0.1495 * exp(-.002875 * CapteurProx[i]) +
                  0.05551 * exp(-.0002107 * CapteurProx[i]);
    }
    float x5 = (dist[5] + rp.robRadius) * cos(rp.theta[5]);
    float y5 = (dist[5] + rp.robRadius) * sin(rp.theta[5]);
    float x6 = (dist[6] + rp.robRadius) * cos(rp.theta[6]);
    float y6 = (dist[6] + rp.robRadius) * sin(rp.theta[6]);
    float x8 = (dist[8] + rp.robRadius) * cos(rp.theta[8]);
    float y8 = (dist[8] + rp.robRadius) * sin(rp.theta[8]);
    float x9 = (dist[9] + rp.robRadius) * cos(rp.theta[9]);
    float y9 = (dist[9] + rp.robRadius) * sin(rp.theta[9]);
    float aLeft, aFront, bLeft, bFront;
    //    printf("points 8 %f %f and 9 %f %f\n",x8,y8,x9,y9);
    if (fabs(x8 - x9) > 0.00001) {
        aFront = (y8 - y9) / (x8 - x9);
        bFront = (x8 * y9 - x9 * y8) / (x8 - x9);
        xRobot = fabs(bFront) / sqrt(1 + aFront * aFront) + 0.025; // TODO FIX
    } else {
        xRobot = x8 + 0.025; // TODO FIX
                             // aFront = (y8-y9)/(sign(x8-x9)*.00001);
                             // bFront = (x8*y9-x9*y8)/(sign(x8-x9)*.00001);
    }
    //    printf("front line a %f b %f \n", aFront, bFront);
    if (fabs(x5 - x6) > 0.00001) {
        aLeft = (y5 - y6) / (x5 - x6);
        bLeft = (x5 * y6 - x6 * y5) / (x5 - x6);
    } else {
        aLeft = (y5 - y6) / (sign(x5 - x6) * .00001);
        bLeft = (x5 * y6 - x6 * y5) / (sign(x5 - x6) * .00001);
    }
    yRobot = fabs(bLeft) / sqrt(1 + aLeft * aLeft) + 0.045; // TODO FIX
    //    printf("left line a %f b %f \n",aFront,bFront);
    //    printf("xRob %f yRob %f\n",xRobot,yRobot);
}
void SendMotorAndLEDCommandToRobot(const char MotorCmd[15]) {
    char MotorAndLEDCommand[23], LEDCommand[9];
    // prepare LED commands
    char Led_1, Led_2, Led_3, Led_4, Led_5, Led_6, Led_7, Led_8;
    Led_1 = '1';
    Led_2 = '1';
    Led_3 = '1';
    Led_4 = '1';
    Led_5 = '1';
    Led_6 = '1';
    Led_7 = '1';
    Led_8 = '1'; // 1 for ON, 0 for OFF
    sprintf(LEDCommand, "%c%c%c%c%c%c%c%c", Led_1, Led_2, Led_3, Led_4, Led_5,
            Led_6, Led_7, Led_8);
    sprintf(MotorAndLEDCommand, "%s%s", LEDCommand, MotorCmd);
    printf("Command to be sent: %s\n", MotorAndLEDCommand);
    int Send;
    Send = sendto(CommandSendingSocket, MotorAndLEDCommand, 23, 0,
                  (struct sockaddr*)&SockaddrinEnvoieCommandes,
                  sizeof(SockaddrinEnvoieCommandes));
    if (Send < 0) {
        printf("Error sending commands\n");
    } else if (Send == 0) {
        printf("Error nothing sent\n");
    }
}
void ReceptionCapteur() {
    int BytesReception;
    socklen_t Size = sizeof(SockaddrinReceptionCapteurs);
    memset(Capteurs, 0, 100);

    struct timeval timeout;
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;
    
    fd_set socks;
    FD_ZERO(&socks);
    FD_SET(SensorReceivingSocket, &socks);
    if (select(SensorReceivingSocket + 1, &socks, NULL, NULL, &timeout)) {
        BytesReception =
            recvfrom(SensorReceivingSocket, Capteurs, 100, 0,
                     (SOCKADDR*)&SockaddrinReceptionCapteurs, &Size);
        if (BytesReception < 0) {
            Capteurs[0] = 0;
            printf("SORRY No Data received\n");
        } else {
            Capteurs[BytesReception] = 0;
            if (BytesReception > 0) {
            } else {
                printf("WARNING No Data received!\n");
            }
        }
    } else {
        printf("TIMEOUT Sensor data reading\n");
    }
}
void DecoupeReception() {
    if (Capteurs[0] == 0) {
        printf("nothing received, no message to manage\n");
        return;
    }
    int SizeBufferto, SizeCapteurs,
        index = -1; // taille des capteurs et emplacement du caractere pour la
                    // d�coupe : index
    char CharEncodeur[2][20], CharProx[10][5];
    char* ptr_pos = NULL;
    char* bufferto;
    /* Preparation de la memoire */
    for (int i = 0; i++; i < 2) {
        memset(CharEncodeur[i], 0, 20);
    }
    for (int i = 0; i++; i < 10) {
        memset(CharProx[i], 0, 5);
    }

    /* Decoupage progressif des valeurs des capteurs */
    bufferto = (char*)calloc(72, sizeof(char));

    ptr_pos = strstr(Capteurs, "q"); // on cherche l'index o� se trouve q
    ptr_pos ? index = ptr_pos - Capteurs + 2 : index = 0;

    memcpy(bufferto, &Capteurs[index], SizeBufferto = 72);
    ptr_pos =
        strstr(bufferto,
               ","); // on cherche l'index o� se trouve la premi�re virgule
    ptr_pos ? index = ptr_pos - bufferto : index = 0;
    memcpy(CharEncodeur[0], bufferto,
           index); // on copie depuis le d�but du Buffer sur une longueur
                   // correspondant � l'index cherch� pr�c�demment

    memcpy(Capteurs, &bufferto[index + 1],
           SizeCapteurs =
               72 - (index + 1)); // on "enleve" la partie d�j� trait�e
    ptr_pos = strstr(Capteurs, "n");
    ptr_pos ? index = ptr_pos - Capteurs : index = 0;
    memcpy(CharEncodeur[1], Capteurs, index);

    memcpy(bufferto, &Capteurs[index + 2],
           SizeBufferto = SizeCapteurs - (index + 2));
    ptr_pos = strstr(bufferto, ",");
    ptr_pos ? index = ptr_pos - bufferto : index = 0;
    memcpy(CharProx[0], bufferto, index);

    memcpy(Capteurs, &bufferto[index + 1],
           SizeCapteurs = SizeBufferto - (index + 1));
    ptr_pos = strstr(Capteurs, ",");
    ptr_pos ? index = ptr_pos - Capteurs : index = 0;
    memcpy(CharProx[1], Capteurs, index);

    memcpy(bufferto, &Capteurs[index + 1],
           SizeBufferto = SizeCapteurs - (index + 1));
    ptr_pos = strstr(bufferto, ",");
    ptr_pos ? index = ptr_pos - bufferto : index = 0;
    memcpy(CharProx[2], bufferto, index);

    memcpy(Capteurs, &bufferto[index + 1],
           SizeCapteurs = SizeBufferto - (index + 1));
    ptr_pos = strstr(Capteurs, ",");
    ptr_pos ? index = ptr_pos - Capteurs : index = 0;
    memcpy(CharProx[3], Capteurs, index);

    memcpy(bufferto, &Capteurs[index + 1],
           SizeBufferto = SizeCapteurs - (index + 1));
    ptr_pos = strstr(bufferto, ",");
    ptr_pos ? index = ptr_pos - bufferto : index = 0;
    memcpy(CharProx[4], bufferto, index);

    memcpy(Capteurs, &bufferto[index + 1],
           SizeCapteurs = SizeBufferto - (index + 1));
    ptr_pos = strstr(Capteurs, ",");
    ptr_pos ? index = ptr_pos - Capteurs : index = 0;
    memcpy(CharProx[5], Capteurs, index);

    memcpy(bufferto, &Capteurs[index + 1],
           SizeBufferto = SizeCapteurs - (index + 1));
    ptr_pos = strstr(bufferto, ",");
    ptr_pos ? index = ptr_pos - bufferto : index = 0;
    memcpy(CharProx[6], bufferto, index);

    memcpy(Capteurs, &bufferto[index + 1],
           SizeCapteurs = SizeBufferto - (index + 1));
    ptr_pos = strstr(Capteurs, ",");
    ptr_pos ? index = ptr_pos - Capteurs : index = 0;
    memcpy(CharProx[7], Capteurs, index);

    memcpy(bufferto, &Capteurs[index + 1],
           SizeBufferto = SizeCapteurs - (index + 1));
    ptr_pos = strstr(bufferto, ",");
    ptr_pos ? index = ptr_pos - bufferto : index = 0;
    memcpy(CharProx[8], bufferto, index);

    memcpy(Capteurs, &bufferto[index + 1],
           SizeCapteurs = SizeBufferto - (index + 1));
    ptr_pos = strstr(Capteurs, "\r");
    ptr_pos ? index = ptr_pos - Capteurs : index = 0;
    memcpy(CharProx[9], Capteurs, index);

    /* conversion des Char en Int */
    for (int i = 0; i < 10; i++) {
        CapteurProx[i] = atoi(CharProx[i]);
    }
    oldEncodeurG = EncodeurG;
    oldEncodeurD = EncodeurD;
    EncodeurG = atoi(CharEncodeur[0]);
    EncodeurD = atoi(CharEncodeur[1]);
}
/**** fonction initialisation Camera ****/
void InitCamera(const std::string& epuck_ip) {
    InitSocketOpening(epuck_ip); // ouverture de la socket d'initialisation
    int Send;
    char CommandeInit[2];
    /* Mise en place de la commande d'init */
    if (CameraActive == true) {
        sprintf(CommandeInit, "1");
    }
    if (CameraActive == false) {
        sprintf(CommandeInit, "0");
    }
    printf("commande Init = %s\n", CommandeInit);
    /* Envoi commande d'init */
    Send = sendto(SockInit, CommandeInit, sizeof(CommandeInit), 0,
                  (struct sockaddr*)&SockaddrinInit, sizeof(SockaddrinInit));
    if (Send < 0) {
        printf("erreur sur l'envoi de l'Init\n");
    } else if (Send == 0) {
        printf("Envoi vide \n");
    }
    CloseSocket(SockInit); // fermeture de la socket d'initialisation
    // HERE (by robin) waiting a little time to avoid synchronizatipon problem
    // with server (socket to receive commands not created yet)
    printf("waiting server to be ready...\n");
    sleep(2);
}
/*****************/
/**** Threads ****/
/*****************/
/**** Thread for receiving camera data ****/
void* CameraReceptionThread(void* arg) {
    printf("Executing thread for receiving camera data\n\r");
    static const int img_msg_size = 57600;
    static const int img_msg_header = 8;
    int num_bytes = -2;
    socklen_t sinsize = sizeof(SockaddrinCamera);
    unsigned char* buffer = (unsigned char*)calloc(
        img_msg_size + img_msg_header, sizeof(unsigned char));
    memset(imgData.msg, 0x0, img_msg_size);
    char idImg_buf[2], idBlock_buf[2];
    memset(idImg_buf, 0, 2);
    memset(idBlock_buf, 0, 2);
    memset(buffer, 0, img_msg_size + img_msg_header);
    int i = 0;
    bool GotData = false;
    
    struct timeval timeout;
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    fd_set socks;
    FD_ZERO(&socks);
    FD_SET(CameraSocket, &socks);
    while (not stop_threads) {
		//int ret = select(CameraSocket + 1, &socks, NULL, NULL, &timeout);
        //if (ret > 0) {
            num_bytes = recvfrom(CameraSocket, (char*)buffer,
                                 img_msg_size + img_msg_header, 0,
                                 (SOCKADDR*)&SockaddrinCamera, &sinsize);
            //printf("received n bytes %d %d times\n",num_bytes,i++);
            if (num_bytes < 0) {
                if (!GotData) {
                    printf("Warning: no bytes received\n");
                    GotData = true;
                }
            } else {
                GotData = false;
                memcpy(idImg_buf, &buffer[0], 2);
                // printf("idimg : %s\n",idImg_buf);
                imgData.id_img = atoi(idImg_buf);
				//std::cout << "id_img: " << imgData.id_img << std::endl;
                memcpy(idBlock_buf, &buffer[3], 1);
                // printf("idBlock : %s\n",idBlock_buf);
                imgData.id_block = atoi(idBlock_buf);
				//std::cout << "id_block: " << imgData.id_block << std::endl;
                memcpy(&imgData.msg[(imgData.id_block) * img_msg_size],
                       &buffer[8], img_msg_size);
				//for(int i=0; i<10; ++i) {
				//	std::cout << (int)buffer[8+i] << " ";
				//}
		std::cout << std::endl;
                num_bytes = -1;
            }
        /*}
		else if(ret < 0) {
			printf("Select error camera thread: %d\n", errno);
		}*/
    }
	printf("Exiting camera thread\n");
    // close the thread
    (void)arg;
    pthread_exit(NULL);
}
// SPECIFIC FUNCTIONS
void SaveData(const string & data_folder) {
    eg = fopen((data_folder + "eg.txt").c_str(), "a+");
    ed = fopen((data_folder + "ed.txt").c_str(), "a+");
    p0 = fopen((data_folder + "p0.txt").c_str(), "a+");
    p1 = fopen((data_folder + "p1.txt").c_str(), "a+");
    p2 = fopen((data_folder + "p2.txt").c_str(), "a+");
    p3 = fopen((data_folder + "p3.txt").c_str(), "a+");
    p4 = fopen((data_folder + "p4.txt").c_str(), "a+");
    p5 = fopen((data_folder + "p5.txt").c_str(), "a+");
    p6 = fopen((data_folder + "p6.txt").c_str(), "a+");
    p7 = fopen((data_folder + "p7.txt").c_str(), "a+");
    p8 = fopen((data_folder + "p8.txt").c_str(), "a+");
    p9 = fopen((data_folder + "p9.txt").c_str(), "a+");
    fprintf(eg, "%i\n", EncodeurG);
    fprintf(ed, "%i\n", EncodeurD);
    fprintf(p0, "%i\n", CapteurProx[0]);
    fprintf(p1, "%i\n", CapteurProx[1]);
    fprintf(p2, "%i\n", CapteurProx[2]);
    fprintf(p3, "%i\n", CapteurProx[3]);
    fprintf(p4, "%i\n", CapteurProx[4]);
    fprintf(p5, "%i\n", CapteurProx[5]);
    fprintf(p6, "%i\n", CapteurProx[6]);
    fprintf(p7, "%i\n", CapteurProx[7]);
    fprintf(p8, "%i\n", CapteurProx[8]);
    fprintf(p9, "%i\n", CapteurProx[9]);
    fclose(ed);
    fclose(eg);
    fclose(p0);
    fclose(p1);
    fclose(p2);
    fclose(p3);
    fclose(p4);
    fclose(p5);
    fclose(p6);
    fclose(p7);
    fclose(p8);
    fclose(p9);
}

/**** Send commands to the wheel motors for 10 seconds****/
void SetWheelCommands(struct timeval startTime, const int& speed_L,
                      const int& speed_R, char MotorCmd[15]) {
    struct timeval curTime;
    gettimeofday(&curTime, NULL);
    long int timeSinceStart =
        ((curTime.tv_sec * 1000000 + curTime.tv_usec) -
         (startTime.tv_sec * 1000000 + startTime.tv_usec)) /
        1000;
    printf("SetWheelCommands\ttimeSinceStart = %ld ms\n", timeSinceStart);
    if (timeSinceStart < 10000) {
        // Sends the command to the epuck motors
        sprintf(MotorCmd, "D,%d,%d", speed_L, speed_R);
    } else {
        sprintf(MotorCmd, "D,%d,%d", 0, 0);
    }
}

/**** Projection mesures infrarouges dans repere robot - fonction specifique de
 * ce programme****/
void ProjectionInfrarougesDansRepereRobotEtSauvetage(const string & data_folder) {
    // infrarouges
    float dist[8];
    for (int i = 0; i < 8; i++) {
        dist[i] = 1.79 * pow(CapteurProx[i], -.681);
    }
    for (int i = 8; i < 10; i++) {
        dist[i] = 0.1495 * exp(-.002875 * CapteurProx[i]) +
                  0.05551 * exp(-.0002107 * CapteurProx[i]);
    }
    dist[6] = dist[6] + 0.01;
    xA = (dist[5] + rp.robRadius) * cos(rp.theta[5]);
    yA = (dist[5] + rp.robRadius) * sin(rp.theta[5]);
    xB = (dist[6] + rp.robRadius) * cos(rp.theta[6]);
    yB = (dist[6] + rp.robRadius) * sin(rp.theta[6]);
    x_A = fopen((data_folder + "xA.txt").c_str(), "a+");
    y_A = fopen((data_folder + "yA.txt").c_str(), "a+");
    x_B = fopen((data_folder + "xB.txt").c_str(), "a+");
    y_B = fopen((data_folder + "yB.txt").c_str(), "a+");
    fprintf(x_A, "%f\n", xA);
    fprintf(y_A, "%f\n", yA);
    fprintf(x_B, "%f\n", xB);
    fprintf(y_B, "%f\n", yB);
    fclose(x_A);
    fclose(y_A);
    fclose(x_B);
    fclose(y_B);
}

string createLogFolder() {
    char folder_name[128];
    time_t current_time;
    struct tm date;

    time(&current_time);
    date = *localtime(&current_time);

    // YYYY_MM_DD-hh:mm:ss
    strftime(folder_name, 128, "../logs/%Y_%m_%d-%X", &date);
    struct stat st = {0};

    std::string log_folder = folder_name;
    if (stat(log_folder.c_str(), &st) == -1) {
        mkdir(log_folder.c_str(), 0777);
    }

    std::string data_log_folder_;
    data_log_folder_ = log_folder + "/data/";
    if (stat(data_log_folder_.c_str(), &st) == -1) {
        mkdir(data_log_folder_.c_str(), 0777);
    }

    images_log_folder = log_folder + "/images/";
    if (stat(images_log_folder.c_str(), &st) == -1) {
        mkdir(images_log_folder.c_str(), 0777);
    }
    return(data_log_folder_);
}

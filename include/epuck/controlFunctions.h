#pragma once

/* CONTROL FUNCTIONS FOR EPUCK ROBOT*/

#include <arpa/inet.h>
#include <cstdio> // Pour les Sprintf
#include <fstream>
#include <iostream>
#include <netinet/in.h>
#include <opencv2/core/core_c.h>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/opencv.hpp>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctime>
#include <unistd.h>
#include <signal.h>


#define INVALID_SOCKET -1
#define SOCKET_ERROR -1

using namespace std;
using namespace cv;

class RobPose
{
public:
    // Constructors
    RobPose(){};
    RobPose(const float & x_, const float & y_, const float & th_) {x = x_; y = y_; th = th_;};
    float getX() const {return x;};
    float getY() const {return y;};
    float getTh() const {return th;};
    void setPose(const float & x_, const float & y_, const float & th_) {x = x_; y = y_; th = th_;};
private:
    float x, y, th;
};

/**** Derive robot pose variation from encoders and save it along with robot linear and angular velocities ****/
RobPose GetCurrPoseFromEncoders(const RobPose & prevRobPose, const int& EncL, const int& EncR, const int& prevEncL, const int& prevEncR, const string & data_folder);

/**** Load image from the disk ****/
Mat LoadAndShowImageFromDisk(const string folderName, const int& cnt_it);

/**** Load encoder measurements from logs on disk ****/
std::vector<int> LoadEncoderLogs(const string folderName);

/**** Draws a map with world frame and robot ****/
void DrawMapWithRobot(const RobPose & rPoseEnc, const RobPose & rPoseVis);

/**** Process image compute and draw barycenter ****/
Point ProcessImageToGetBarycenter(const Mat& colRawImg);

/**** Send commands (speed_L, speed_R) to motors in order to realize robot
 * velocities (v, w) for 10 seconds ****/
void SetRobotVelocities(struct timeval startTime, const float& vel,
                        const float& omega, char MotorCmd[15]);

// control robot using images from the camera
void ControlRobotWithVisualServoing(struct timeval startTime,
                                    char MotorCmd[15]);
// make robot follow a wall using infrared measurements for ten seconds
void ControlRobotToFollowWall(struct timeval startTime, char MotorCmd[15]);
